package ua.danit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class App {

    static List<People> selected =  new ArrayList<People>();

    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();

        HelloServlet helloServlet = new HelloServlet();
        helloServlet.setSelected(selected);
        ServletHolder holder = new ServletHolder(helloServlet);
        handler.addServlet(holder, "/*");

        Selected messagesServlet = new Selected();
        messagesServlet.setSelected(selected);
        ServletHolder messagesHolder = new ServletHolder(messagesServlet);
        handler.addServlet(messagesHolder, "/messages");

        Login loginServlet = new Login();
        handler.addServlet(new ServletHolder(loginServlet),"/login");

/*chat page*/

        ChatG goToChat = new ChatG();
        goToChat.setSelected(selected);
        ServletHolder chatHolder = new ServletHolder(goToChat);
        handler.addServlet(chatHolder, "/chat");

/*end of chat page code*/

        DBUtilTinder worker = new DBUtilTinder();

        server.setHandler(handler);
        server.start();
        server.join();
    }

    static class HelloServlet extends HttpServlet {

        static List<People> list =  new ArrayList<People>();
        List<People> selected;
        static int cur = 0;

        public void setSelected(List<People> selected) {
            this.selected = selected;
        }


        @Override
        public void init() throws ServletException {
            People one = new People(1,"name 1", " ");
            People one9 = new People(2,"name 2", "https://orig00.deviantart.net/5025/f/2011/357/5/8/mad_men__s_sexy_joan_holloway_by_c_marie-d26foh3.jpg");
            People one7 = new People(3, "name 3", "https://orig00.deviantart.net/4087/f/2009/284/4/4/self_portrait_in_black_by_c_marie.jpg");
            People one2 = new People(4, "name 4", "https://pre00.deviantart.net/bf46/th/pre/f/2012/306/c/4/c43d7aa6bdda8b5500b2f19b2a22434e-d5jr701.jpg");
            People one3 = new People(5,"name 1", "https://pre00.deviantart.net/d19c/th/pre/i/2013/131/6/b/freckles_by_martasyrko-d64wz67.jpg");
            People one4 = new People(6, "name 1", "https://pre00.deviantart.net/873b/th/pre/f/2011/065/a/e/mad_men___betty___by_adonihs-d3b1rtc.jpg");

            list.add(one);
            list.add(one9);
            list.add(one7);
            list.add(one2);
            list.add(one3);
            list.add(one4);
        }

        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, java.io.IOException {
            PrintWriter out = resp.getWriter();

            out.write("<html><body>");

//         People person = list.get(cur++ % list.size());
            People person = list.get(cur++ / 6);
            out.write("<img src='" + person.picUrl + "' width=\"500px\" height=\"500px\">");
            out.write("<h1>" + person.name + "</h1>");
            out.write("<form action='/' method='POST'>");
            out.write("<p style=\"text-align: center\">");
            out.write("<input name='yesAnsw' type='hidden' value='YES'>");
            out.write("<input name='personId' type='hidden' value='"+ person.id+"'>");
            out.write("<input name='personName' type='hidden' value='"+ person.name+"'>");
            out.write("<input name='personPic' type='hidden' value='"+ person.picUrl+"'>");
            out.write("<p></p>");
            out.write("<button type='submit'>Yes</button>");
            out.write("</form>");

            out.write("<form action='/' method='POST'>");
            out.write("<input name='noAnsw' type='hidden'>");
            out.write("<button type='submit'>No</button>");
            out.write("</form>");

            out.write("</body></html>");

        }


        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException {

            String noAnsw = req.getParameter("noAnsw");
            int personId = req.getIntHeader("personId");
            String personName = req.getParameter("personName");
            String personPic = req.getParameter("personPic");

            if (noAnsw == null)
                selected.add(new People(personId, personName, personPic));

            System.out.println(selected);
            resp.sendRedirect("/");
        }
    }
}