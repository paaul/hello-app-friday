package ua.danit;

import java.util.Date;

public class MessageT {

    String to;
    String from;
    String text;
    Date date;

    public MessageT(String to, String from, String text, Date date) {
        this.text = text;
        this.from = from;
        this.to = to;
        this.date = date;
    }
}
