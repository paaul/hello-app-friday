package ua.danit;

import java.sql.*;

public class DBUtilTinder {

    public static void DBUtil() throws SQLException {


        String DB_URL = "jdbc:mysql://localhost:3306/tinder"; //url доступа к базе
        String USER = "root";
        String PASSWORD = "root";

        Connection con = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        String sql = "SELECT ID, NAME, PICURL FROM TINDERAPP";
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            Long id = resultSet.getLong("ID");
            String name = resultSet.getString("NAME");
            String picurl = resultSet.getString("PICURL");
            System.out.printf("Product {id :%d, name: %s, picurl: %s}\n", id, name, picurl);

        }
        resultSet.close();
        statement.close();
        con.close();
    }
}
