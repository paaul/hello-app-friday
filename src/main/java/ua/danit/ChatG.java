package ua.danit;

import com.sun.xml.internal.ws.api.message.Message;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

public class ChatG extends HttpServlet {

    List<People> selected;

    public void setSelected(List<People> selected) {
        this.selected = selected;
    }

    List<String> chatList = new ArrayList<>();

    Date date = new Date();


    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, java.io.IOException {
        PrintWriter out2 = resp.getWriter();

        MessageT msg = new MessageT("Jen", "Kate", "hello", date);
        MessageT msg2 = new MessageT("Male", "Kite", "hello to y", date);

        People person = new People(1,"That one guy", "https://img00.deviantart.net/66e1/i/2012/239/3/6/girl_by_martasyrko-d5cmll0.jpg");
        People person2 = new People(2,"Girl", "https://img00.deviantart.net/66e1/i/2012/239/3/6/girl_by_martasyrko-d5cmll0.jpg");

        out2.write("<html>" +
                "<head>" +
                "<style>" +
                ".message_from {\n" +
                "\n" +
                "    background-color: olivedrab;\n" +
                "    text-align: left;\n" +
                "    border-radius: 5px;\n" +
                "\n" +
                "}\n" +
                "\n" +
                ".message_to {\n" +
                "\n" +
                "    background-color: blueviolet;\n" +
                "    text-align: right;\n" +
                "    border-radius: 5px;\n" +
                "}" +
                ".msg {\n" +
                "\n" +
                "    background-color: saddlebrown;\n" +
                "    text-align: center;\n" +
                "    border-radius: 5px;\n" +
                "}" +
                "</style>" +
                "</head>");
        out2.write("<body>");

        out2.write("<p style=\"text-align: center\">");
        out2.write("<img src='" + person.picUrl + "' width=\"200px\" height=\"200px\" draggable=\"false\"/></div>");
        out2.write("</p>");

        for (String msgId : chatList) {
            out2.write("<div>");
            out2.write("<center>");
            out2.write("<div class=\"avatar\">");
            out2.write("<div class=\"message_from\">"+ msg.from + " to " + msg.to + "</div>");
//            out2.write("<div class=\"message_to\"" + msg2.from + " to " + msg2.to + "\">" + msgId + "</div>");
//            out2.write("<div class=\"name\"> '" + person.name + "' to '" + person2.name + "'</div>");
            out2.write("<div class=\"msg\">");
            out2.write("<p>'" + msg.text + "'</p>");
            out2.write("</div>");
            out2.write("</center>");
            out2.write("</div>");
        }

        out2.write("<center>");
        out2.write("<form action='/chat' method='POST'>");
        out2.write("<input name='chatList' type='text'>");
        out2.write("<p style=\"text-align: center\">");
        out2.write("<button type='submit'>Chat</button>");
        out2.write("</p>");
        out2.write("</form>");
        out2.write("</center>");

        out2.write("</body></html>");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String chat = req.getParameter("chatList");
        chatList.add(chat);

        resp.sendRedirect(req.getRequestURI());
    }
}