package ua.danit;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "login", loadOnStartup = 1, urlPatterns = "/login")
public class Login extends HttpServlet {

    public static Map<String, String> knownUsers = new HashMap<String, String>() {{
        put("Ivan", "i");
        put("John", "j2018");
    }};

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.write("<html><body>");
        out.write("  <form action='/login' method='POST'>");
        out.write("    <label>User Name</label>");
        out.write("    <input name='userName' type='text'><br>");

        out.write("    <label>Password</label>");
        out.write("    <input name='password' type='password'><br>");

        out.write("    <button type='submit'>Login</button>");
        out.write("  </form>");
        out.write("</body></html>");
    }

    public static Map<String, String> tokens = new HashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        PrintWriter out = resp.getWriter();
        boolean known = knownUsers.containsKey(userName)
                && knownUsers.get(userName).equals(password);

        String token = UUID.randomUUID().toString();
        tokens.put(token, userName);
        resp.addCookie(new Cookie("user-token", token));


//        resp.getWriter().write(known ? "I know you " + userName +  : "I don't know you."); // тут иф поставить

        resp.sendRedirect("/");




    }
}