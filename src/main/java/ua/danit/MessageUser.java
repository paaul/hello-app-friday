package ua.danit;

public class MessageUser {

    String name;
    String message;

    public MessageUser (String name, String message) {
        this.message = message;
        this.name = name;
    }
}
