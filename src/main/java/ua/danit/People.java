package ua.danit;

public class People  {

    int id;
    String name;
    String picUrl;

    public People(int id, String name, String picUrl) {
        this.id = id;
        this.name = name;
        this.picUrl = picUrl;

    }

    public People(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", picUrl='" + picUrl + '\'' +
                '}';
    }



}
