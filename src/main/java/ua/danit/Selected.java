package ua.danit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Selected extends HttpServlet {

    List<People> selected;

    public void setSelected(List<People> selected) {
        this.selected = selected;
    }
       

        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, java.io.IOException {
            PrintWriter out1 = resp.getWriter();

            out1.write("<html><body>");

            for (People item : selected) {

                out1.write("<img src='" + item.picUrl + "' width=\"500px\" height=\"500px\">");
                out1.write("<h1>" + item.name + "</h1>");

                out1.write("<form action='/messages?name='" + item.name + "' method='POST'>");
                out1.write("<input name='chat' type='hidden' value='CHAT'>");
                out1.write("<button type='submit'>CHAT</button>");
                out1.write("</form>");

            }
            out1.write("</body></html>");

        }


        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException {




            resp.sendRedirect("/messages");
        }

}
